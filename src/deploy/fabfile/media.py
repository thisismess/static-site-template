from fabric.api import *


def push_uploads():
    local('rsync -avh %s/ %s@%s:%s' % (env.local_uploads_path, env.user, env.hosts[0], env.remote_uploads_path))
    

def pull_uploads():
    local('rsync -avh %s@%s:%s/ %s/' % (env.user, env.hosts[0], env.remote_uploads_path, env.local_uploads_path))
    

def sync_uploads():
    pull_uploads()
    push_uploads()