import os, re

from time import gmtime, strftime
from contextlib import closing
from zipfile import ZipFile, ZIP_DEFLATED

from fabric.contrib.files import exists, upload_template
from fabric.contrib.project import upload_project
from fabric.context_managers import path, shell_env
from fabric.utils import indent as ind, abort, warn
from fabric.colors import *
from fabric.api import *
from fabric.contrib.project import rsync_project
from contextlib import contextmanager as _contextmanager

from fabtools import require as needs
from fabtools.python import virtualenv

import fabtools

from environments import *
from database import *
from media import *
from server import *
from env import *
from opbeat import *

from .utils import select_env
from .serverdensity import configure_serverdensity


def log(text, indent=0, color=white):
    print(color(ind(text, indent*4)))


def pull():
    if 'db' not in env.keys():
        select_env()
    execute(pull_database)
    execute(sync_uploads)
    

def push():
    if 'db' not in env.keys():
        select_env()
    execute(push_database)
    execute(sync_uploads)
    

def ship():
    if 'branch' not in env.keys():
        select_env()
    execute(release)
    execute(prune)
    
    
def mark():
    if 'branch' not in env.keys():
        select_env()
    register_deployment(env.repo_root)


def release():
    """
    Create a new release on the server specified.
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        sudo('pwd') # We cheat, and prompt for the password first
    log("Creating new release on %s..." % env.host)
    check_environment(1)
    release = strftime('%Y%m%d%H%M%S', gmtime())
    env.release = release
    upload_release(release, 0)
    if 'django_app' in env:
        configure_virtualenv(release, 0)
        collect_static(release, 0)
    configure(release, 0)


def check_environment(indent=0):
    log("Checking environment on %s..." % env.host, indent)
    check_filesystem(indent)
    # check_postgres(indent+1) # Something about CentOS breaks this. Investigate later. Sucks :(
    if 'django_app' in env:
        check_redis(indent)
        check_virtualenv(indent)
    

def check_filesystem(indent=0):
    log("Checking filesystem...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        cwd, env.cwd = env.cwd, '/' # we have to swap the cwd cause Fabric prepends it to all commands, and it might not exist yet.
        needs.directory(cwd, owner=env.user, use_sudo=True)
        env.cwd, cwd  = cwd, None # unhack
        needs.directory(os.path.join(env.cwd, 'backups'), owner=env.user, use_sudo=False, mode=755)
        needs.directory(os.path.join(env.cwd, 'releases'), owner=env.user, use_sudo=False, mode=755)
        needs.directory(os.path.join(env.cwd, 'logs'), owner=env.user, use_sudo=False, mode=777)
        needs.directory(os.path.join(env.cwd, 'bin'), owner=env.user, use_sudo=False, mode=777)
        needs.directory(os.path.join(env.cwd, 'uploads'), owner=env.user, use_sudo=False, mode=777)
    log("Filesystem is OK", indent)


def check_postgres(indent=0):
    """
    Checks for the presence of the database.
    """
    log("Checking Postgres...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        needs.postgres.server()
        needs.postgres.database(env.db, env.db_user)
    log("Postgres is OK", indent)
    

def check_redis(indent):
    log("Checking Redis...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        needs.service.started('redis')
    log("Redis is OK", indent)
    

def check_virtualenv(indent=0):
    if 'django_app' in env:
        log("Checking Virtualenv...", indent)
        with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):    
            needs.python.virtualenv(env.remote_virtualenv)
            with virtualenv(env.remote_virtualenv):
                needs.python.package('Django')
                needs.python.package('uwsgi')
        log("Virtualenv is OK", indent)
    else:
        log("Virtualenv skipped. No Django app present.", indent)
    
    
def upload_release(release, indent=0):
    log("Creating new release %s..." % release, indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        # archive project to local tmp dir
        local('mkdir /tmp/%s' % release)
        log("Archiving git repo..", indent)
        local('git archive %s %s | tar -x -C /tmp/%s --strip=1 %s' % (env.branch, env.django_app_path, release, env.django_app_path))
        with cd(env.remote_release_path):
            run("mkdir %s" % release)
            # copy previous release to new dir for rsync
            if exists("current") and run('ls %s/current' % env.remote_release_path):
                run("cp -R current/* %s" % release)
            # rsync local tmp archive with previous release copy
            log("Syncing archive to server..", indent)
            rsync_project(remote_dir='%s/%s' % (env.remote_release_path, release), local_dir='/tmp/%s/%s/' % (release, env.local_name), delete=True)
            log("Archive uploaded", indent)
            
            
def collect_static(release='../public', indent=0):
    log("Collecting static files...", indent)
    with virtualenv(env.remote_virtualenv):
        with cd(env.remote_release_path):
            with shell_env(**env.setenv):
                run( "rm -rf %s/static/*" % release)
                run( 'python %s/manage.py collectstatic --noinput' % release)
    log("Static files are OK", indent)
    
    
def migrate_database(release, indent=0):
    log("Migrating database...", indent)
    with settings():
        with virtualenv(env.remote_virtualenv):
            with cd(env.remote_release_path):
                with shell_env(**env.setenv):
                    run( 'python %s/manage.py syncdb' % release)
                    run( 'python %s/manage.py migrate' % release)
    log("Database migration OK", indent)

    
def configure(release=None, indent=0):
    if not release:
        release = current().split('/')[-1]
        env.release = release
    if 'django_app' in env:
        configure_virtualenv(release, indent)
        migrate_database(release, indent)
        configure_uwsgi(release, indent)
        if env.uses_celeryd:
            configure_celery(release, indent)
    configure_nginx(release, indent)
    configure_serverdensity(release, indent)
    start(indent)
    

def configure_nginx(release=None, indent=0):
    if not release:
        release = current().split('/')[-1]
        env.release = release
    log("Configuring nginx...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        if 'django_app' in env:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.django_app, 'static')
        else:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.project, 'static')
        needs.nginx.site(env.project, template_source=env.local_nginx_config, **dict(env))
        sudo('chmod 775 %s' % env.remote_statics_path)
    log("Nginx is OK", indent)
    

def configure_uwsgi(release, indent=0):
    log("Configuring Uwsgi...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_wsgi_path = os.path.join(env.remote_release_path, env.release, env.project)
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_wsgi_ini_path = os.path.join(env.remote_python_path, 'uwsgi.ini')
        env.uwsgi_env = generate_uwsgi_env()
        upload_template(env.local_uwsgi_ini, '/tmp/%s.uwsgi.ini' % release, context=env)
        upload_template(env.local_uwsgi_config, '/tmp/%s.uwsgi.conf' % release, context=env)
        sudo('mv /tmp/%s.uwsgi.ini  %s' % (release, env.remote_wsgi_ini_path))
        sudo('mv /tmp/%s.uwsgi.conf /etc/init/%s.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s.conf' % env.project)
    log("Uwsgi is OK", indent)


def configure_celery(release, indent=0):
    log("Configuring Celery...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_project_path = os.path.join(env.remote_release_path, env.release)
        upload_template(env.local_celery_config, '/tmp/%s' % release, context=env)
        sudo('mv /tmp/%s /etc/init/%s-celery.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s-celery.conf' % env.project)
    log("Celery is OK", indent)
    
    
def configure_virtualenv(release, indent=0):
    log("Installing python packages...", indent)
    print env.remote_virtualenv
    with virtualenv(env.remote_virtualenv):    
        env.remote_requirements_file = os.path.join(env.remote_release_path, env.release, 'requirements.txt')
        needs.python.requirements(env.remote_requirements_file)
    log("Python packages OK...", indent)


def current():
    files = get_releases()
    return files[0]
    

def get_releases():
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        files = run('ls -a %s' % env.remote_release_path)
        files = re.split("[\s]+", files)
        files = filter(lambda f: re.match("^[\d]+", f), files)
        files = map(lambda f: "%s/%s" % (env.remote_release_path, f), files)
        files.sort()
    return files
    

def prune(indent=0):
    log("Pruning old releases...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):    
        files = get_releases()
        keeping = 0 - int(env.keep_releases)
        del files[keeping:]
        for f in files:
            sudo("rm -rf %s" % f)    
    log("%s Releases pruned" % len(files), indent)