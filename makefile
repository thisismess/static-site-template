all: web

libjpeg:
	test -d .venv

virtualenv:
	test -d .venv || virtualenv .venv
	. .venv/bin/activate; pip install fabric==1.8.1 fabtools
	touch .venv/bin/activate
	
web: virtualenv

clean:
	rm -rf .venv
	
run: 
	. .venv/bin/activate; cd src/web && python -m SimpleHTTPServer
	
ship:
	. .venv/bin/activate; fab -f src/deploy/fabfile ship
	
mark:
	. .venv/bin/activate; fab -f src/deploy/fabfile mark